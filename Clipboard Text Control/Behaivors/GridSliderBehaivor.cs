﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Runtime.Serialization;
using Suima.Settings;

namespace CTC.Behaivors {

    public class GridSliderBehaivor : Behavior<Grid> {

        #region イベント購読の登録・解除

        /// <summary>
        /// 登録
        /// </summary>
        protected override void OnAttached() {

            base.OnAttached();

            SettingMgr.Current.Saving += (s, e) => {
                Get();
            };
        
            Set();

        }

        /// <summary>
        /// 解除
        /// </summary>
        protected override void OnDetaching() {

            base.OnDetaching();

        }

        #endregion

        private void Set() {

            var Setting = SettingMgr.Current.GetSetting<GridSliderSetting>();

            if (Setting.GridSetting == null) { return; }

            for (int i = 0; i < this.AssociatedObject.ColumnDefinitions.Count; i++) {

                this.AssociatedObject.ColumnDefinitions[i].Width = Setting.GridSetting[i].GetGridLength();

            }

        }

        private void Get() {

            var Setting = SettingMgr.Current.GetSetting<GridSliderSetting>();

            Setting.GridSetting = this.AssociatedObject.ColumnDefinitions.Select( x => new S_GridLength( x.Width ) ).ToArray();

        }

    }

    [Setting]
    [DataContract]
    public class GridSliderSetting {

        [DataMember]
        public S_GridLength[] GridSetting { get; set; } = null;

    }

    [DataContract]
    public struct S_GridLength {

        public S_GridLength(GridLength data) {
            this.Value = data.Value;
            this.GridUnitType = data.GridUnitType;
        }

        [DataMember]
        public double Value { get; set; }

        [DataMember]
        public GridUnitType GridUnitType { get; set; }

        public GridLength GetGridLength() {
            return new GridLength( this.Value, this.GridUnitType );
        }

    }

}
