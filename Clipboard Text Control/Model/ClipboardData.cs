﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using Suima.Extensions;
using static Suima.Windows.Win32.User32;
using System.Windows.Media.Imaging;
using System.IO;

namespace CTC.Model {
    public class ClipboardData : DataInfos {

        #region Static

        public static ClipboardData Empty = new ClipboardData( -1 );

        /// <summary>
        /// Clipboardのデータをコピーし、新たなInstanceを作る
        /// </summary>
        /// <returns></returns>
        public static ClipboardData MakeInstance() {

            return MakeInstance( Clipboard.GetDataObject() );

        }

        /// <summary>
        /// IDataObject を元に新たなInstanceを作る
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ClipboardData MakeInstance(IDataObject source) {
            return MakeInstance( source, DateTime.Now );
        }

        /// <summary>
        /// IDataObject を元に新たなInstanceを作る
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ClipboardData MakeInstance(IDataObject source, DateTime t) {

            if (source.GetFormats( false ).Length == 0) {
                return Empty;
            }

            ClipboardData data = null;

            if (source.GetDataPresent( typeof( DataInfos ) )) {
                var d = source.GetData( typeof( DataInfos ) );
                if (d as DataInfos != null) {
                    data = new ClipboardData( d as DataInfos );
                } else {
                    data = new ClipboardData();
                }
            } else {
                if (source as DataInfos != null) {
                    data = new ClipboardData( (DataInfos)source );
                } else {
                    data = new ClipboardData();
                }
            }

            source.CopyTo( data.Data,
                           (_) => 
                           data.IsCopyFailed = true,
                           (f) => f != typeof( DataInfos ).ToString() );


            data.SequenceNumber = GetClipboardSequenceNumber();
            data.FirstTime = t;

            return data;

        }

        #endregion

        #region Constructor

        protected ClipboardData() : base() {
            this.Data = new DataObject();
            this.Date = DateTime.Now;
        }

        protected ClipboardData(DataInfos info) : base( info ) {
            this.Data = new DataObject();
            this.Date = DateTime.Now;
        }

        protected ClipboardData(ClipboardData input) : base( input ) {
            this.Data = input.Data;
            this.Date = DateTime.Now;
        }

        protected ClipboardData(int id) : base( id ) {
            this.Data = new DataObject();
            this.Date = DateTime.Now;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 保存してあるデータ本体
        /// </summary>
        public DataObject Data { get; protected set; }
        public DateTime Date { get; protected set; }

        public bool IsText { get => this.Data.ContainsText(); }
        public bool IsImage { get => this.Data.ContainsImage(); }
        public bool IsAudio { get => this.Data.ContainsAudio(); }
        public bool IsOther {
            get => !this.IsText &&
                   !this.IsImage &&
                   !this.IsAudio;
        }

        public string Text { get => this.Data.GetText(); }
        public BitmapSource Image { get => this.Data.GetImage(); }
        public Stream Audio { get => this.Data.GetAudioStream(); }

        public string[] Formats { get => this.Data.GetFormats( false ); }

        public string DateString { get => this.Date.ToLongTimeString(); }

        #endregion

        #region Method

        public ClipboardData FormatsRemove( IEnumerable<string> RmFormats ) {

            DataObject data = new DataObject();
            DateTime t = this.FirstTime;

            IEnumerable<string> formats = this.Formats.Difference( RmFormats );

            this.Data.SelectFormatCopyTo( formats, data );

            return MakeInstance( data );

        }

        public DataInfos GetDataInfos() {
            return new DataInfos( this );
        }

        public override string ToString() {

            if (this.Data.ContainsText()) {
                return this.Data.GetText().RegexReplace( @"[ \s]+", @" " );//.Round(50, "...");
            } else {
                return $"ID:{this.ID} {this.Data.GetFormats( false ).FirstOrDefault()}";
            }

        }

        #endregion

    }
}
