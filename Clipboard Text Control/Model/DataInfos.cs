﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CTC.Model {

    [Serializable]
    public class DataInfos {

        private static int BaseID = 0;

        public DataInfos() {
            this.IsCopyFailed = false;
            this.SequenceNumber = 0;
            this.ID = BaseID++;
            this.FirstTime = DateTime.Now;
        }

        public DataInfos( DataInfos obj ) {
            this.IsCopyFailed   = obj.IsCopyFailed;
            this.SequenceNumber = obj.SequenceNumber;
            this.ID             = obj.ID;
            this.FirstTime      = obj.FirstTime;

            if ( obj.ID >= BaseID ) {
                BaseID = obj.ID + 1;
            }
        }

        protected DataInfos( int id ) {
            this.IsCopyFailed = false;
            this.SequenceNumber = 0;
            this.ID = id;
            this.FirstTime = new DateTime();
        }

        /// <summary>
        /// コピーに失敗したかどうか
        /// </summary>
        public bool IsCopyFailed { get; protected set; }

        /// <summary>
        /// Clipboardに付随するSequenceNumber
        /// </summary>
        public uint SequenceNumber { get; protected set; }

        /// <summary>
        /// データを識別する為の数値
        /// </summary>
        public int ID { get; protected set; }

        /// <summary>
        /// IDを付与した最初の時間を記録する
        /// </summary>
        public DateTime FirstTime { get; protected set; }

    }
}
