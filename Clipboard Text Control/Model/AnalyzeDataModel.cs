﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Collections.ObjectModel;
using Suima.Extensions;
using Livet;

namespace CTC.Model {

    public class AnalyzeDataModel : NotificationObject {

        #region Properties

        public ClipboardData Data { get; }

        #endregion



        public AnalyzeDataModel(ClipboardData input) {

            this.Data = input;

            RaisePropertyChanged( nameof( Data ) );
        }



        public void MakeData(IEnumerable<string> rms) {

            System.Diagnostics.Debug.WriteLine( "rms" );
            foreach (string f in rms) {
                System.Diagnostics.Debug.WriteLine( $"     {f}" );
            }
            if (rms.Any()) {

                DataObject data = new DataObject();

                IEnumerable<string> formats = this.Data.Formats.Difference( rms );

                this.Data.Data.SelectFormatCopyTo( formats.ToArray(), data );

                Services.HistoryService.Current.SetHistory( ClipboardData.MakeInstance( data ) );
                Services.HistoryService.Current.CurrentSave();

            }

        }

    }
}
