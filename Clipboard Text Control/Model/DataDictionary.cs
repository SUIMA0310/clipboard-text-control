﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Suima.Extensions;

namespace CTC.Model {
    public class DataDictionary : IDataObject {

        protected struct MyData {
            public object obj;
            public bool convert;
        }

        protected Dictionary<string, MyData> _dic = null;

        #region Constructor

        public DataDictionary() {
            _dic = new Dictionary<string, MyData>();
        }

        public DataDictionary(IDataObject input) {

            if (input is DataDictionary) {

                this._dic = ( input as DataDictionary )?._dic;

            } else {

                input.CopyTo( this );

            }

        }

        #endregion

        #region Public Method

        public void RemoveData(string format) {

            _dic.Remove( format );

        }

        #endregion

        #region DataObject

        #region Contains

        public bool ContainsText() {
            return GetDataPresent( DataFormats.UnicodeText, false );
        }

        public bool ContainsImage() {
            return GetDataPresent( DataFormats.Bitmap,      false );
        }

        public bool ContainsAudio() {
            return GetDataPresent( DataFormats.WaveAudio,   false );
        }

        public bool ContainsFileDropList() {
            return GetDataPresent( DataFormats.FileDrop,    false );
        }

        #endregion

        #region Get

        public string GetText() {
            return GetData( DataFormats.UnicodeText, false ) as string ?? string.Empty;
        }

        public BitmapSource GetImage() {
            return GetData( DataFormats.Bitmap, true ) as BitmapSource;
        }

        public Stream GetAudioStream() {
            return GetData( DataFormats.WaveAudio, false ) as Stream;
        }

        public StringCollection GetFileDropList() {
            StringCollection fileDropListCollection;
            string[] fileDropList;

            fileDropListCollection = new StringCollection();

            fileDropList = GetData( DataFormats.FileDrop, true ) as string[];
            if (fileDropList != null) {
                fileDropListCollection.AddRange( fileDropList );
            }

            return fileDropListCollection;
        }

        #endregion

        #endregion

        #region Interface

        #region Get

        #region GetData

        public object GetData(string format) {
            return GetData( format, true );
        }

        public object GetData(string format, bool autoConvert) {

            if (!_dic.ContainsKey( format )) {

                if (autoConvert) {

                    throw new NotImplementedException( "変換機能は提供されません" );

                } else {

                    return null;

                }

            }

            return _dic[format].obj;

        }

        #endregion

        #region GetDataPresent

        public bool GetDataPresent(string format) {
            return GetDataPresent( format, true );
        }

        public bool GetDataPresent(string format, bool autoConvert) {

            if (_dic.ContainsKey( format )) {
                return true;
            }

            if (autoConvert) {
                throw new NotImplementedException();
            }

            return false;

        }

        #endregion

        #region GetFormats

        public string[] GetFormats(bool autoConvert) {

            if (autoConvert) {
                throw new NotImplementedException();
            }

            return _dic.Keys.ToArray();

        }

        #endregion

        #endregion

        #region Set

        public void SetData(object data) {
            SetData( data.GetType().FullName, data );
        }

        public void SetData(string format, object data) {
            SetData( format, data, true );
        }

        public void SetData(string format, object data, bool autoConvert) {

            _dic[format] = new MyData { obj = data, convert = autoConvert };

        }

        #endregion

        #endregion

        #region Not Support

        public object GetData(Type format) {
            throw new NotImplementedException();
        }

        public bool GetDataPresent(Type format) {
            throw new NotImplementedException();
        }

        public string[] GetFormats() {
            throw new NotImplementedException();
        }

        public void SetData(Type format, object data) {
            throw new NotImplementedException();
        }

        #endregion

    }
}
