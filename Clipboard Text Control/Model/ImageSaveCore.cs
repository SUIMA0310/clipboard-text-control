﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Livet;
using Suima.Wpf.Dialogs;

namespace CTC.Model {

    public enum SaveFolder {
        Desktop,
        MyPictures,
        MyDocuments,
        User
    }

    public enum SaveFormat {
        JPEG,
        PNG,
        BMP,
        TIFF
    }

    public interface IImageSaveSetting {

        string SavePath { get; }

    }

    public class ImageSaveCore : NotificationObject {

        public enum Overwrite {
            None,
            Overwrite,
            Rename,
            Cancel
        }

        private IImageSaveSetting setting;

        public ImageSaveCore(IImageSaveSetting setting) {
            this.setting = setting ?? throw new ArgumentNullException();
        }

        public bool ShowErrorDialog { get; set; } = true;

        public Overwrite OverwriteBehavior { get; set; } = Overwrite.None;

        public bool Save(BitmapSource Image) {

            string Path;
            SaveFormat Format;
            BitmapEncoder Encoder;

            try {

                Path = setting.SavePath;

                Format = SelectFormat( ref Path );

                if (!CheckFile( ref Path )) {

                    return false;

                }

            } catch {

                if (ShowErrorDialog) {

                    MessageBox.Show(
                        "指定されたパスは利用できません。",
                        "警告",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning
                        );

                    return false;

                } else {

                    throw;

                }

            }

            try {

                Encoder = SelectEncoder( Format );

                using (
                    var stream = new FileStream( Path, FileMode.Create )
                    ) {

                    Encoder.Frames.Add( BitmapFrame.Create( Image ) );
                    Encoder.Save( stream );

                }

            } catch {

                if (ShowErrorDialog) {

                    MessageBox.Show(
                    "保存に失敗しました。",
                    "警告",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                    );

                    return false;

                } else {

                    throw;

                }

            }

            return true;

        }

        protected BitmapEncoder SelectEncoder(SaveFormat format) {

            switch (format) {
                case SaveFormat.BMP:
                    return new BmpBitmapEncoder();
                case SaveFormat.PNG:
                    return new PngBitmapEncoder();
                case SaveFormat.TIFF:
                    return new TiffBitmapEncoder();
                case SaveFormat.JPEG:
                    return new JpegBitmapEncoder();
                default:
                    throw new ArgumentException( "formatの値が不正" );
            }

        }

        protected SaveFormat SelectFormat(ref string path) {

            if (Path.HasExtension( path )) {

                var extension = Path.GetExtension( path );

                switch (extension.ToLower()) {

                    case ".jpeg":
                        return SaveFormat.JPEG;
                    case ".png":
                        return SaveFormat.PNG;
                    case ".bmp":
                        return SaveFormat.BMP;
                    case ".tiff":
                        return SaveFormat.TIFF;
                    default:
                        throw new ArgumentException( "Pathが不正" );

                }

            } else {

                path = Path.ChangeExtension( path, ".png" );
                return SaveFormat.PNG;

            }

        }

        protected bool CheckFile(ref string path) {

            if (File.Exists( path )) {

                DialogResult ret = DialogResult.Cancel;

                switch (this.OverwriteBehavior) {
                    case Overwrite.Overwrite:
                        ret = DialogResult.Yes;
                        break;
                    case Overwrite.Rename:
                        ret = DialogResult.No;
                        break;
                    case Overwrite.Cancel:
                        ret = DialogResult.Cancel;
                        break;

                    case Overwrite.None: {
                            ret = MessageBox.Show(
                                "上書きしますか？",
                                "確認",
                                MessageBoxButtons.YesNoCancel,
                                MessageBoxIcon.Warning
                                );
                            break;
                        }

                    default:
                        throw new ArgumentException();
                }



                if (ret == DialogResult.Cancel) {
                    return false;
                }

                if (ret == DialogResult.No) {
                    var serialFileName = new Suima.IO.SerialFileName( path );
                    path = serialFileName.GetPath();
                }

            }

            var directoryInfo = Directory.GetParent( path );

            if (!directoryInfo.Exists) {
                directoryInfo.Create();
            }

            return true;

        }

    }
}
