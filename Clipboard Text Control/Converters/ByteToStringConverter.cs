﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace CTC.Converters {
    public class ByteToStringConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {

            long Byte = (long)value;

            //GB 以上
            if (Byte >= 1073741824) {
                return $"{( Byte / 1073741824 ).ToString( "N" )}GB";
            }

            //MB 以上
            if (Byte >= 1048576) {
                return $"{( Byte / 1048576 ).ToString( "N" )}MB";
            }

            //KB 以上
            if (Byte >= 1024) {
                return $"{( Byte / 1024 ).ToString( "N" )}KB";
            }

            return $"{( Byte ).ToString( "N" )}Byte";

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }

    }
}
