﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroTrilithon.Mvvm;

namespace CTC.ViewModel {
    public class DialogViewModel : WindowViewModel {

        public DialogViewModel() {
            this.DialogResult = false;
        }

        //表示するメッセージを設定する。
        public string Text { get; set; }

        //OKボタンが押されたとき
        public void OK() {
            this.DialogResult = true;
            this.Close();
        }

        //Cancelボタンが押されたとき
        public void Cancel() {
            this.DialogResult = false;
            this.Close();
        }

    }
}
