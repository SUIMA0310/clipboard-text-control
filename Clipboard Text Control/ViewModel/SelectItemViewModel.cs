﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroTrilithon.Mvvm;
using CTC.Model;

namespace CTC.ViewModel {
    public class SelectItemViewModel<T> : WindowViewModel {

        public SelectItemViewModel( IEnumerable<T> List ) {
            this.List = List.ToArray();
        }

        public T[] List { get; }

        public ObservableCollection<T> SelectedList { get; } = new ObservableCollection<T>();

        public void OK() {
            this.Close();
        }

        public void Cancel() {
            SelectedList.Clear();
            this.Close();
        }

    }
}
