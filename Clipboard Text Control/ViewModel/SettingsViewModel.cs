﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTC.ViewModel {

    public class SettingsViewModel : Livet.ViewModel {

        public SettingsViewModel() {

            this.SystemCheckViewModel    = new Settings.SystemCheckViewModel();
            this.ThemeChangeViewModel    = new Settings.ThemeChangeViewModel();
            this.HistorySettingViewModel = new Settings.HistorySettingViewModel();

        }

        #region ViewModels

        public Settings.SystemCheckViewModel    SystemCheckViewModel    { get; }
        public Settings.ThemeChangeViewModel    ThemeChangeViewModel    { get; }
        public Settings.HistorySettingViewModel HistorySettingViewModel { get; }

        #endregion

    }

}
