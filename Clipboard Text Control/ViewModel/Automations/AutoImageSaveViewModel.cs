﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.ViewModel.Analyzes;
using Livet.EventListeners;
using MetroTrilithon.Mvvm;
using Suima.Settings;

namespace CTC.ViewModel.Automations {
    public class AutoImageSaveViewModel : ImageSaveViewModel {

        private Services.AutoImageSaveService Service;

        public AutoImageSaveViewModel() {
            this.Service = Services.AutoImageSaveService.Current;

            var l = new PropertyChangedEventListener( this.Service ).AddTo(this);
            l.RegisterHandler(
                nameof( this.Service.IsEnabled ),
                (s, e) => {
                    RaisePropertyChanged( nameof( IsEnabled ) );
                }
                );

            this.setting = SettingMgr.Current.GetSetting<Services.AutoImageSaveServiceSetting>( false );
            if (this.setting == null) {
                SettingMgr.Current.Loaded += (s, e) => {
                    this.setting = SettingMgr.Current.GetSetting<Services.AutoImageSaveServiceSetting>();
                };
            }
        }

        public bool IsEnabled {
            get => this.Service.IsEnabled;
            set => this.Service.IsEnabled = value;
        }

    }
}
