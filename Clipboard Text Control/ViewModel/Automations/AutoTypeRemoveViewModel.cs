﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Livet.Messaging;
using Livet.EventListeners;
using CTC.Services;
using Suima.Extensions;

namespace CTC.ViewModel.Automations {
    public class AutoTypeRemoveViewModel : Livet.ViewModel {

        private AutoTypeRemoveService service = AutoTypeRemoveService.Current;

        public AutoTypeRemoveViewModel() {

            #region Event Listener

            var l = new PropertyChangedEventListener( service );

            l.RegisterHandler(
                nameof( service.IsEnabled ),
                (s, e) => {
                    RaisePropertyChanged( nameof( IsEnabled ) );
                }
            );

            l.RegisterHandler(
                nameof( service.FormatList ),
                (s, e) => {
                    RaisePropertyChanged( nameof( FormatList ) );
                }
            );

            #endregion

        }

        public bool IsEnabled {
            get => service.IsEnabled;
            set => service.IsEnabled = value;
        }

        public ObservableCollection<string> FormatList {
            get => service.FormatList;
        }

        public ObservableCollection<string> SelectedList { get; }
            = new ObservableCollection<string>();

        public void Remove() {

            var temp = new string[SelectedList.Count];
            SelectedList.CopyTo( temp, 0 );

            foreach (var item in temp) {
                FormatList.Remove( item );
            } 

        }

        public void SelectFormats() {

            var formats = HistoryService.Current.CurrentData.Formats;

            if (formats    == null) { return; }
            if (FormatList == null) { return; }

            var vm = new SelectItemViewModel<string>( formats.Difference( FormatList ) );

            Messenger.Raise(
                new TransitionMessage( typeof( View.Automations.SelectFormatWindow ),
                                      vm,
                                      TransitionMode.Modal,
                                      "Window.Transition.Child" ) );

            foreach (var item in vm.SelectedList) {
                FormatList.Add( item );
            }

        }

    }
}
