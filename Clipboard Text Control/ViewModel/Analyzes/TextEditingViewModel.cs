﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using CTC.Model;

namespace CTC.ViewModel.Analyzes {
    public class TextEditingViewModel : Livet.ViewModel {

        #region Field

        private ClipboardData _data = null;
        private string _EditingText = string.Empty;
        private bool _TextChanged = false;

        #endregion

        public TextEditingViewModel(ClipboardData data) {
            this._data = data;
            this._EditingText = data.Text;
            RaisePropertyChanged( nameof(EditingText) );
        }

        #region Properties

        public string EditingText {
            get => this._EditingText;
            set {
                if (this._EditingText == value) { return; }
                this._EditingText = value;
                this.TextChanged = true;
                RaisePropertyChanged();
            }
        }

        public bool TextChanged {
            get => this._TextChanged;
            set {
                if (this._TextChanged == value) { return; }
                this._TextChanged = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Method

        public void Complete() {

            DataObject obj = new DataObject();
            DateTime t = this._data.FirstTime;
            obj.SetText( this.EditingText );
            var data = ClipboardData.MakeInstance( obj, t );
            Services.HistoryService.Current.SetHistory( data );
            Services.HistoryService.Current.CurrentSave();

        }

        #endregion

    }
}
