﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CTC.Model;
using Livet.EventListeners;
using MetroTrilithon.Mvvm;
using Suima.Extensions;

namespace CTC.ViewModel.Analyzes {
    public class TypeRemoveViewModel : Livet.ViewModel {

        #region Properties

        private ClipboardData Data { get; }

        #endregion

        public TypeRemoveViewModel(ClipboardData input) {

            this.SelectedFormats = new ObservableCollection<string>();
            this.Data = input;

            var listener = new CollectionChangedEventListener( this.SelectedFormats ).AddTo( this );
            listener.RegisterHandler( (s, e) => {
                RaisePropertyChanged( nameof( CanRemove ) );
            } );

            RaisePropertyChanged( nameof( Data ) );
            RaisePropertyChanged( nameof( IsCopyFailed ) );
            RaisePropertyChanged( nameof( Formats ) );
            RaisePropertyChanged( nameof( SelectedFormats ) );

        }

        public bool IsCopyFailed { get => this.Data.IsCopyFailed; }
        public string[] Formats { get => this.Data.Formats; }
        public ObservableCollection<string> SelectedFormats { get; }

        public bool CanRemove { get => this.SelectedFormats.Count > 0; }

        public void MakeData() {

            this.MakeData( this.SelectedFormats );

        }
        public void MakeData(IEnumerable<string> rms) {

            if (rms.Any()) {

                Services.HistoryService.Current
                    .SetHistory( this.Data.FormatsRemove( rms ) );

                Services.HistoryService.Current
                    .CurrentSave();

            }

        }

    }
}
