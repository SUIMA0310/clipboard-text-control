﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.Model;

namespace CTC.ViewModel.Analyzes {
    public class DataInfoViewModel : Livet.ViewModel {

        private ClipboardData Data = null;

        public DataInfoViewModel( ClipboardData data ) {
            this.Data = data;
        }

        public int ID {
            get => this.Data.ID;
        }
        public uint SequenceNumber {
            get => this.Data.SequenceNumber;
        }
        public bool IsCopyFailed {
            get => this.Data.IsCopyFailed;
        }
        public string DataType {
            get {

                if ( this.Data.IsText ) {
                    return "Text";
                }
                if ( this.Data.IsImage ) {
                    return "Image";
                }
                if ( this.Data.IsAudio ) {
                    return "Audio";
                }

                return "Other";
            }
        }
        public string Time {
            get => this.Data.Date.ToString();
        }
        public string FirstTime {
            get => this.Data.FirstTime.ToString();
        }

    }
}
