﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.Model;
using Suima.Settings;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace CTC.ViewModel.Analyzes {
    public class ImageSaveViewModel : Livet.ViewModel {

        protected ImageSaveSetting setting;
        protected ClipboardData data;

        public ImageSaveViewModel( ClipboardData data ) {
            this.setting = SettingMgr.Current.GetSetting<ImageSaveSetting>();
            this.data = data;
        }

        protected ImageSaveViewModel() {

        }

        #region SaveFolder

        public bool IsDesktop {
            get => setting.Folder == SaveFolder.Desktop;
            set {
                if (value) {
                    setting.Folder = SaveFolder.Desktop;
                    RaisePropertyChanged( nameof( IsDesktop ) );
                    RaisePropertyChanged( nameof( IsMyPictures ) );
                    RaisePropertyChanged( nameof( IsMyDocuments ) );
                    RaisePropertyChanged( nameof( IsUser ) );
                }
                
            }
        }

        public bool IsMyPictures {
            get => setting.Folder == SaveFolder.MyPictures;
            set {
                if (value) {
                    setting.Folder = SaveFolder.MyPictures;
                    RaisePropertyChanged( nameof( IsDesktop ) );
                    RaisePropertyChanged( nameof( IsMyPictures ) );
                    RaisePropertyChanged( nameof( IsMyDocuments ) );
                    RaisePropertyChanged( nameof( IsUser ) );
                }
                
            }
        }

        public bool IsMyDocuments {
            get => setting.Folder == SaveFolder.MyDocuments;
            set {
                if (value) {
                    setting.Folder = SaveFolder.MyDocuments;
                    RaisePropertyChanged( nameof( IsDesktop ) );
                    RaisePropertyChanged( nameof( IsMyPictures ) );
                    RaisePropertyChanged( nameof( IsMyDocuments ) );
                    RaisePropertyChanged( nameof( IsUser ) );
                }
            }
        }

        public bool IsUser {
            get => setting.Folder == SaveFolder.User;
            set {
                if (value) {
                    setting.Folder = SaveFolder.User;
                    RaisePropertyChanged( nameof( IsDesktop ) );
                    RaisePropertyChanged( nameof( IsMyPictures ) );
                    RaisePropertyChanged( nameof( IsMyDocuments ) );
                    RaisePropertyChanged( nameof( IsUser ) );
                }
            }
        }

        #endregion

        public string FileName {
            get => setting.FileName;
            set {
                if ( value == setting.FileName ) { return; }
                setting.FileName = value;
                RaisePropertyChanged();
            }
        }

        public string UserFolder {
            get => setting.UserPath;
            set {
                if (value == setting.UserPath) { return; }
                setting.UserPath = value;
                RaisePropertyChanged();
            }
        }

        public void Save() {

            var ins = new ImageSaveCore( setting );
            if( ins.Save( data.Image )) {
                Services.StatusBarService.Current.SetInfomationMsg(
                    "保存に成功しました。");
            } else {
                Services.StatusBarService.Current.SetInfomationMsg(
                    "保存に失敗しました。" );
            }

        }

        public void Reference() {

            var dialog = new FolderBrowserDialog();

            dialog.Description = "フォルダを指定してください。";

            if (dialog.ShowDialog() == DialogResult.OK) {
                this.UserFolder = dialog.SelectedPath;
            }

        }

    }

    [Setting]
    [DataContract]
    public class ImageSaveSetting : IImageSaveSetting {

        [DataMember]
        public SaveFolder Folder { get; set; } = SaveFolder.Desktop;

        [DataMember]
        public string UserPath { get; set; } = string.Empty;

        [DataMember]
        public string FileName { get; set; } = "image.png";

        public string SavePath => System.IO.Path.Combine( GetPath( Folder ), FileName );

        protected string GetPath(SaveFolder f) {

            switch (f) {

                case SaveFolder.Desktop:
                    return Environment.GetFolderPath(
                        Environment.SpecialFolder.Desktop );

                case SaveFolder.MyDocuments:
                    return Environment.GetFolderPath(
                        Environment.SpecialFolder.MyDocuments );

                case SaveFolder.MyPictures:
                    return Environment.GetFolderPath(
                        Environment.SpecialFolder.MyPictures );


                case SaveFolder.User:
                    return UserPath;

                default:
                    throw new ArgumentException();

            }

        }
    }
}
