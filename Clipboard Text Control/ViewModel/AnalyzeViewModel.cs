﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.Services;
using CTC.Model;
using Suima.Extensions;
using Livet.Commands;
using Livet.EventListeners;
using MetroTrilithon.Lifetime;
using MetroTrilithon.Mvvm;
using System.Collections.ObjectModel;

namespace CTC.ViewModel {
    public class AnalyzeViewModel : Livet.ViewModel {

        public AnalyzeViewModel() {

            #region Listener SetUp

            var listener = new PropertyChangedEventListener( HistoryService.Current ).AddTo( this );
            listener.RegisterHandler( nameof( HistoryService.Current.CurrentData ), (s, e) => CurrentUpdata() );

            #endregion

        }

        private void CurrentUpdata() {

            var data = HistoryService.Current.CurrentData;

            DataInfoViewModelUpdata( data );
            TypeRemoveViewModelUpdata( data );
            TextEditingViewModelUpdata( data );
            ImageSaveViewModelUpdata(data);

        }

        #region DataInfo

        private void DataInfoViewModelUpdata(ClipboardData data) {

            if ( data == null ) {
                this.DataInfoViewShown = false;
                return;
            }

            if (data == ClipboardData.Empty) {
                this.DataInfoViewShown = false;
                return;
            }

            this.Disposing( this.DataInfoViewModel );
            this.DataInfoViewModel = new Analyzes.DataInfoViewModel( data );
            this.DataInfoViewShown = true;

        }

        private bool _DataInfoViewShown = false;
        public Analyzes.DataInfoViewModel _DataInfoViewModel = null;

        public bool DataInfoViewShown {
            get => this._DataInfoViewShown;
            private set {
                if (this._DataInfoViewShown == value) { return; }
                this._DataInfoViewShown = value;
                RaisePropertyChanged();
            }
        }
        public Analyzes.DataInfoViewModel DataInfoViewModel {
            get => this._DataInfoViewModel;
            private set {
                if (this._DataInfoViewModel == value) { return; }
                this._DataInfoViewModel = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region TypeRemove

        private void TypeRemoveViewModelUpdata(ClipboardData data) {

            if (!data.Data.GetFormats( false ).Any()) {
                this.TypeRemoveViewShown = false;
                return;
            }

            this.Disposing( this.TypeRemoveViewModel );
            this.TypeRemoveViewModel = new Analyzes.TypeRemoveViewModel( data );
            this.TypeRemoveViewShown = true;

        }

        private bool _TypeRemoveViewShown = false;
        public Analyzes.TypeRemoveViewModel _TypeRemoveViewModel = null;

        public bool TypeRemoveViewShown {
            get => this._TypeRemoveViewShown;
            private set {
                if (this._TypeRemoveViewShown == value) { return; }
                this._TypeRemoveViewShown = value;
                RaisePropertyChanged();
            }
        }
        public Analyzes.TypeRemoveViewModel TypeRemoveViewModel {
            get => this._TypeRemoveViewModel;
            private set {
                if (this._TypeRemoveViewModel == value) { return; }
                this._TypeRemoveViewModel = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region TextEditing

        private void TextEditingViewModelUpdata(ClipboardData data) {

            if (!data.IsText) {
                this.TextEditingViewShown = false;
                return;
            }

            this.Disposing( this.TextEditingViewModel );
            this.TextEditingViewModel = new Analyzes.TextEditingViewModel( data );
            this.TextEditingViewShown = true;

        }

        private bool _TextEditingViewShown = false;
        public Analyzes.TextEditingViewModel _TextEditingViewModel = null;

        public bool TextEditingViewShown {
            get => this._TextEditingViewShown;
            private set {
                if (this._TextEditingViewShown == value) { return; }
                this._TextEditingViewShown = value;
                RaisePropertyChanged();
            }
        }
        public Analyzes.TextEditingViewModel TextEditingViewModel {
            get => this._TextEditingViewModel;
            private set {
                if (this._TextEditingViewModel == value) { return; }
                this._TextEditingViewModel = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region ImageSave

        private void ImageSaveViewModelUpdata(ClipboardData data) {

            if (!data.IsImage) {
                this.ImageSaveViewShown = false;
                return;
            }

            this.Disposing( this.ImageSaveViewModel );
            this.ImageSaveViewModel = new Analyzes.ImageSaveViewModel( data );
            this.ImageSaveViewShown = true;

        }

        private bool _ImageSaveViewShown = false;
        public Analyzes.ImageSaveViewModel _ImageSaveViewModel = null;

        public bool ImageSaveViewShown {
            get => this._ImageSaveViewShown;
            private set {
                if (this._ImageSaveViewShown == value) { return; }
                this._ImageSaveViewShown = value;
                RaisePropertyChanged();
            }
        }
        public Analyzes.ImageSaveViewModel ImageSaveViewModel {
            get => this._ImageSaveViewModel;
            private set {
                if (this._ImageSaveViewModel == value) { return; }
                this._ImageSaveViewModel = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        private void Disposing( Livet.ViewModel vm ) {

            if ( vm == null ) {
                return;
            }

            if ( vm is IDisposable ) {

                ( (IDisposable)vm ).Dispose();

            }

        }

    }
}
