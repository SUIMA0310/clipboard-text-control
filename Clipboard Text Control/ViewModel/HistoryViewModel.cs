﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Livet;
using CTC.Services;
using Suima.Extensions;
using Livet.Commands;
using Livet.EventListeners;
using MetroTrilithon.Lifetime;
using MetroTrilithon.Mvvm;

namespace CTC.ViewModel {
    public class HistoryViewModel : Livet.ViewModel {

        public HistoryViewModel() {

            this.SelectCommand = new ListenerCommand<Model.ClipboardData>( (d) => this.Select( d ) );
            this.RemoveCommand = new ListenerCommand<Model.ClipboardData>( (d) => this.Remove() );

            var listener = new PropertyChangedEventListener( HistoryService.Current ).AddTo( this );
            listener.RegisterHandler( 
                        nameof( HistoryService.Current.DispatcherHistory ), 
                        ( s, e ) => RaisePropertyChanged( nameof( this.ClipboardHistory ) ) );
            listener.RegisterHandler(
                        nameof( HistoryService.Current.CurrentData ),
                        (s, e) => RaisePropertyChanged( nameof( this.SelectedData ) ) );

        }

        public IList<Model.ClipboardData> ClipboardHistory {
            get => HistoryService.Current.DispatcherHistory;
        }

        public Model.ClipboardData SelectedData {
            get => HistoryService.Current.CurrentData;
            set => HistoryService.Current.CurrentData = value;
        }

        public ListenerCommand<Model.ClipboardData> SelectCommand { get; }
        public ListenerCommand<Model.ClipboardData> RemoveCommand { get; }

        private void Select( Model.ClipboardData data ) {
            HistoryService.Current.CurrentSave();
        }

        public void Remove() {
            HistoryService.Current.History.Remove(SelectedData);
            HistoryService.Current.CurrentData = Model.ClipboardData.Empty;
        }

    }
}
