﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTC.ViewModel {
    public class AutomationViewModel : Livet.ViewModel {

        public AutomationViewModel() {

            this.AutoTypeRemoveViewModel = new Automations.AutoTypeRemoveViewModel();
            this.AutoImageSaveViewModel  = new Automations.AutoImageSaveViewModel();

        }

        public Automations.AutoTypeRemoveViewModel AutoTypeRemoveViewModel { get; private set; }
        public Automations.AutoImageSaveViewModel  AutoImageSaveViewModel  { get; private set; }

    }
}
