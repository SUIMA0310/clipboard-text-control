﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.Services;
using Suima.Settings;
using Suima.Wpf.Dialogs;
using System.Runtime.Serialization;
using Livet.EventListeners;

namespace CTC.ViewModel.Settings {

    public class HistorySettingViewModel : Livet.ViewModel {

        private HistoryService Service;
        private PropertyChangedEventListener Event;

        public HistorySettingViewModel() {

            Service = HistoryService.Current;
            Event = new PropertyChangedEventListener( Service );

            Event.RegisterHandler(
                nameof( Service.IsMaxCountEnabled ),
                (s, e) => { RaisePropertyChanged(nameof( IsMaxCountEnabled ) ); }
                );
            Event.RegisterHandler(
                nameof( Service.MaxCount ),
                (s, e) => { RaisePropertyChanged( nameof( MaxCount ) ); }
                );
        }

        public bool IsMaxCountEnabled {
            get => Service.IsMaxCountEnabled;
            set{
                if (Service.IsMaxCountEnabled == value ) { return; }
                Service.IsMaxCountEnabled = value;
                RaisePropertyChanged();
            }
        }

        public int MaxCount {
            get => Service.MaxCount;
            set {
                if (Service.MaxCount == value) { return; }
                Service.MaxCount = value;
                RaisePropertyChanged();
            }
        }

        public void AllRemove() {

            var ret = MessageBox.Show(
                "全ての履歴を削除します。",
                "確認",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning,
                MessageBoxDefaultButton.Button2
                );

            if ( ret == DialogResult.OK ) {
                Service.History.Clear();
            }

        }

    }

    [Setting]
    [DataContract]
    public class TestClass {

        [DataMember]
        public int TestData { get; set; } = 100;

    }

}
