﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using MetroRadiance.UI;
using Suima.Settings;
using System.Runtime.Serialization;

namespace CTC.ViewModel.Settings {
    public class ThemeChangeViewModel : Livet.ViewModel {

        private ThemeChangeSetting setting = SettingMgr.Current.GetSetting<ThemeChangeSetting>();

        public ThemeChangeViewModel() {

            WindowsTheme = false;
            Dark = false;
            Light = false;

            WindowsAccent = false;
            Purple = false;
            Blue = false;
            Orange = false;
            Red = false;

            switch (setting.Theme) {
                case ThemeEnum.WindowsTheme:
                    WindowsTheme = true;
                    break;
                case ThemeEnum.Dark:
                    Dark = true;
                    break;
                case ThemeEnum.Light:
                    Light = true;
                    break;
            }

            switch (setting.Accent) {
                case AccentEnum.WindowsAccent:
                    WindowsAccent = true;
                    break;
                case AccentEnum.Purple:
                    Purple = true;
                    break;
                case AccentEnum.Blue:
                    Blue = true;
                    break;
                case AccentEnum.Orange:
                    Orange = true;
                    break;
                case AccentEnum.Red:
                    Red = true;
                    break;
            }

        }

        #region WindowsTheme 変更通知プロパティ

        private bool _WindowsTheme = ThemeService.Current.Theme == Theme.Windows;

        public bool WindowsTheme {
            get { return this._WindowsTheme; }
            set {
                if (this._WindowsTheme != value) {
                    this._WindowsTheme = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeTheme( Theme.Windows );
                        setting.Theme = ThemeEnum.WindowsTheme;
                    }
                }
            }
        }

        #endregion

        #region Light 変更通知プロパティ

        private bool _Light = ThemeService.Current.Theme == Theme.Light;

        public bool Light {
            get { return this._Light; }
            set {
                if (this._Light != value) {
                    this._Light = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeTheme( Theme.Light );
                        setting.Theme = ThemeEnum.Light;
                    }
                }
            }
        }

        #endregion

        #region Dark 変更通知プロパティ

        private bool _Dark = ThemeService.Current.Theme == Theme.Dark;

        public bool Dark {
            get { return this._Dark; }
            set {
                if (this._Dark != value) {
                    this._Dark = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeTheme( Theme.Dark );
                        setting.Theme = ThemeEnum.Dark;
                    }
                }
            }
        }

        #endregion

        #region WindowsAccent 変更通知プロパティ

        private bool _WindowsAccent = ThemeService.Current.Accent.SyncToWindows;

        public bool WindowsAccent {
            get { return this._WindowsAccent; }
            set {
                if (this._WindowsAccent != value) {
                    this._WindowsAccent = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeAccent( Accent.Windows );
                        setting.Accent = AccentEnum.WindowsAccent;
                    }
                }
            }
        }

        #endregion

        #region Purple 変更通知プロパティ

        private bool _Purple = ThemeService.Current.Accent.Specified == Accent.SpecifiedColor.Purple;

        public bool Purple {
            get { return this._Purple; }
            set {
                if (this._Purple != value) {
                    this._Purple = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeAccent( Accent.Purple );
                        setting.Accent = AccentEnum.Purple;
                    }
                }
            }
        }

        #endregion

        #region Blue 変更通知プロパティ

        private bool _Blue = ThemeService.Current.Accent.Specified == Accent.SpecifiedColor.Blue;

        public bool Blue {
            get { return this._Blue; }
            set {
                if (this._Blue != value) {
                    this._Blue = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeAccent( Accent.Blue );
                        setting.Accent = AccentEnum.Blue;
                    }
                }
            }
        }

        #endregion

        #region Orange 変更通知プロパティ

        private bool _Orange = ThemeService.Current.Accent.Specified == Accent.SpecifiedColor.Orange;

        public bool Orange {
            get { return this._Orange; }
            set {
                if (this._Orange != value) {
                    this._Orange = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeAccent( Accent.Orange );
                        setting.Accent = AccentEnum.Orange;
                    }
                }
            }
        }

        #endregion

        #region Red 変更通知プロパティ

        private bool _Red = ThemeService.Current.Accent.Color == Colors.Red;

        public bool Red {
            get { return this._Red; }
            set {
                if (this._Red != value) {
                    this._Red = value;
                    this.RaisePropertyChanged();

                    if (value) {
                        ThemeService.Current.ChangeAccent( Accent.FromColor( Colors.Red ) );
                        setting.Accent = AccentEnum.Red;
                    }
                }
            }
        }

        #endregion

    }

    public enum ThemeEnum {
        WindowsTheme,
        Light,
        Dark
    }

    public enum AccentEnum {
        WindowsAccent,
        Purple,
        Blue,
        Orange,
        Red
    }

    [Setting]
    [DataContract]
    public class ThemeChangeSetting {
        
        [DataMember]
        public ThemeEnum Theme = ThemeEnum.Dark;

        [DataMember]
        public AccentEnum Accent = AccentEnum.Blue;

    }
}
