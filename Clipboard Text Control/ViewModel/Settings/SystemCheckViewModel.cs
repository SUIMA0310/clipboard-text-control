﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Livet;

namespace CTC.ViewModel.Settings {
    public class SystemCheckViewModel : Livet.ViewModel {

        private DispatcherTimer dispatcherTimer = null;

        public SystemCheckViewModel() {
            this.dispatcherTimer = new DispatcherTimer( DispatcherPriority.Normal );
            this.dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
            this.dispatcherTimer.Tick += (s, e) => {
                RaisePropertyChanged( nameof(UsingMemory) );
            };
            this.dispatcherTimer.Start();
        }

        public string UsingMemory {
            get => byteString( Environment.WorkingSet );
        }

        private string byteString( long Byte) {

            //GB 以上
            if (Byte >= 1073741824) {
                return $"{( (double)Byte / 1073741824.0 ).ToString( "N" )}GB";
            }

            //MB 以上
            if (Byte >= 1048576) {
                return $"{( (double)Byte / 1048576.0 ).ToString( "N" )}MB";
            }

            //KB 以上
            if (Byte >= 1024) {
                return $"{( (double)Byte / 1024.0 ).ToString( "N" )}KB";
            }

            return $"{( Byte ).ToString( "N" )}Byte";

        }

    }
}
