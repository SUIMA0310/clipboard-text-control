﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using Livet;
using MetroTrilithon.Mvvm;

namespace CTC.ViewModel {

    public class MainWindowViewModel : WindowViewModel {

        public MainWindowViewModel() {
            this.Title = "Clipboard Viewer";
        }

        public void Deactive() {
            this.SendWindowAction( Livet.Messaging.Windows.WindowAction.Minimize );
        }

        #region Properties

        public HistoryViewModel    HistoryViewModel    { get; set; }
        public AnalyzeViewModel    AnalyzeViewModel    { get; set; }
        public SettingsViewModel   SettingsViewModel   { get; set; }
        public AutomationViewModel AutomationViewModel { get; set; }

        #endregion

    }

}
