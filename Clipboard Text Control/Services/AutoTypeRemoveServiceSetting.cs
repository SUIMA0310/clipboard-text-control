﻿using Suima.Settings;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace CTC.Services {
    [Setting]
    [DataContract]
    public class AutoTypeRemoveServiceSetting : Base.AutomaticBaseSetting {

        [DataMember]
        public ObservableCollection<string> FormatList { get; set; }
            = new ObservableCollection<string>();

    }
}
