﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using Suima.Extensions;
using Suima.Windows;
using Suima.Windows.Win32;
using System.Windows.Forms;

namespace CTC.Services {
    public class HotkeyMgrService : Base.IService, IDisposable {

        #region Static

        private static HotkeyMgrService _Current;
        public static HotkeyMgrService Current {
            get => _Current ?? ( _Current = new HotkeyMgrService() );
        }

        #endregion

        private Dictionary<KeyCombo, HotKey> KeyDic = new Dictionary<KeyCombo, HotKey>();
        private Queue<KeyCombo> RegisterQueue = new Queue<KeyCombo>();
        private bool IsInitialize = false;
        private Window _UseWindow = null;

        public Window UseWindow {
            get => _UseWindow;
            set {
                if ( IsInitialize ) {
                    throw new NotSupportedException("初期化後の変更はサポートしていません。");
                }
                _UseWindow = value ?? throw new ArgumentNullException();
            }
        }

        public HotkeyMgrService() {

        }

        public void Initialize() {
            this.IsInitialize = true;
            if ( _UseWindow == null ) {
                _UseWindow = System.Windows.Application.Current.MainWindow;
            }
            Dequeue();
        }

        public void Register(ModKeys ModKey, Keys Key, EventHandler Event) {

            Register(
                new KeyCombo {
                    ModKey = ModKey,
                    Key = Key,
                    handler = Event
                } );

        }


        private void Register(KeyCombo key) {

            if (IsInitialize) {

                HotKey ins;
                if ( KeyDic.ContainsKey(key) ) {

                    KeyDic[key].Push += key.handler;

                } else {

                    ins = new HotKey( UseWindow, key.ModKey, key.Key );
                    ins.Push += key.handler;
                    KeyDic.Add( key, ins );

                }
                

            } else {

                RegisterQueue.Enqueue( key );

            }

        }

        private void Dequeue() {

            RegisterQueue.Do( x => Register( x ) );

        }

        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    this.KeyDic.Values.Do( x => x.Dispose() );
                    this.KeyDic.Clear();
                }
                disposedValue = true;
            }
        }

        public void Dispose() {
            Dispose( true );
        }

        #endregion

        private struct KeyCombo {
            public ModKeys ModKey;
            public Keys Key;
            public EventHandler handler;

            public override bool Equals(object obj) {

                if ( obj == null ) { return false; }
                if ( !(obj is KeyCombo) ) { return false; }

                var ins = (KeyCombo)obj;

                return this.ModKey == ins.ModKey
                    && this.Key    == ins.Key;

            }

            public override int GetHashCode() {
                return base.GetHashCode();
            }

        }

    }
}
