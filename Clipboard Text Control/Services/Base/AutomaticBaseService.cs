﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Suima.Settings;
using Livet;

namespace CTC.Services.Base {
    public abstract class AutomaticBaseService<T> 
        : NotificationObject, IService 
        where T : class, IAutomaticBaseSetting {

        protected HistoryService History { get; private set; }
        protected T Setting { get; private set; }

        protected AutomaticBaseService() {
            this.History = HistoryService.Current;
            this.Setting = SettingMgr.Current.GetSetting<T>(false);
            if ( this.Setting == null ) {
                SettingMgr.Current.Loaded += (s, e) => {
                    this.Setting = SettingMgr.Current.GetSetting<T>();
                };
            }
        }

        public void Initialize() {
            this.History.Updata += HistryUpdata;
            this.InitializeCore();
        }

        protected abstract void InitializeCore();

        public bool IsEnabled {
            get => Setting?.IsEnabled ?? false;
            set {
                if (Setting != null) {
                    if (Setting.IsEnabled == value) { return; }
                    Setting.IsEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool InWork { get; set; }

        private void HistryUpdata(object s, EventArgs e) {
            if (!IsEnabled) { return; }
            if (!InWork) {
                InWork = true;
                this.HistryUpdataCore( this.History.CurrentData );
                InWork = false;
            }
        }

        protected abstract void HistryUpdataCore( Model.ClipboardData data );

    }
}
