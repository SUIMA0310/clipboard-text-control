﻿using System.Runtime.Serialization;

namespace CTC.Services.Base {

    public interface IAutomaticBaseSetting {

        bool IsEnabled { get; set; }

    }

    [DataContract]
    public abstract class AutomaticBaseSetting : IAutomaticBaseSetting {

        [DataMember]
        public bool IsEnabled { get; set; } = false;

    }

}