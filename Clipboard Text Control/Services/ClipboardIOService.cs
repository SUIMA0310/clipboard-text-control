﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.Model;
using Livet;
using MetroTrilithon.Lifetime;
using Suima.Windows;
using Suima.Extensions;
using static Suima.Windows.Win32.User32;

namespace CTC.Services {
    public class ClipboardIOService : NotificationObject, IDisposableHolder, Base.IService {

        public static ClipboardIOService Current { get; } = new ClipboardIOService();

        private uint _WaitTime = 1000;
        private DateTime _LastUpdataTime = new DateTime();
        private ClipboardMonitoring clipboardMonitoring;
        private ClipboardData CurrentClipboardData = ClipboardData.Empty;

        private ClipboardIOService() {

        }

        public void Initialize() {
            this.clipboardMonitoring = ClipboardMonitoring.Create( Application.Current.MainWindow ).AddTo( this );
            this.clipboardMonitoring.Update += (s) => {
                ClipboardUpdate();
            };
        }

        public event EventHandler Copy;

        private void OnCopy() {
            Copy?.Invoke( this, null );
        }

        private void ClipboardUpdate() {
            DateTime now = DateTime.Now;
            var time = now - LastUpdataTime;
            LastUpdataTime = now;
            if ( time.TotalSeconds < (double)WaitTime / 1000.0 ) {
                return;
            }
            OnCopy();
            RaisePropertyChanged( nameof( Clipboard ) );
        }

        public ClipboardData Clipboard {
            get => GetClipboardData();
            set => SetClipboardData( value );
        }

        public ClipboardData GetClipboardData() {

            GC.Collect( GC.MaxGeneration );

            if ( this.CurrentClipboardData == ClipboardData.Empty) {
                return ( this.CurrentClipboardData = ClipboardData.MakeInstance() );
            }

            if ( GetClipboardSequenceNumber() == this.CurrentClipboardData.SequenceNumber ) {
                return this.CurrentClipboardData;
            }

            return ( this.CurrentClipboardData = ClipboardData.MakeInstance() );

        }

        public void SetClipboardData( ClipboardData value ) {

            DataObject data = new DataObject();

            if ( !value.Data.GetFormats().Any() ) {
                System.Windows.Clipboard.Clear();
                return;
            }

            value.Data.CopyTo( data );
            data.SetData( value.GetDataInfos() );

            System.Windows.Clipboard.SetDataObject( data, false );

            data = null;
            GC.Collect( GC.MaxGeneration );

        }

        public uint WaitTime {
            get => this._WaitTime;
            set {
                if ( this._WaitTime == value ) { return; }
                this._WaitTime = value;
                RaisePropertyChanged();
            }
        }
        public DateTime LastUpdataTime {
            get => this._LastUpdataTime;
            set {
                if (this._LastUpdataTime == value) { return; }
                this._LastUpdataTime = value;
                RaisePropertyChanged();
            }
        }

        public static bool HaveData {
            get => System.Windows.Clipboard.GetDataObject().GetFormats().Any();
        }

        #region IDisposable members

        private readonly LivetCompositeDisposable compositeDisposable = new LivetCompositeDisposable();

        ICollection<IDisposable> IDisposableHolder.CompositeDisposable => this.compositeDisposable;

        void IDisposable.Dispose() {
            this.compositeDisposable.Dispose();
        }

        #endregion

    }
}
