﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Livet;

namespace CTC.Services {
    public class StatusBarService : NotificationObject, Base.IService {

        public static StatusBarService Current { get; } = new StatusBarService();

        private string PermMsg { get; set; }
        private string ShowMsg { get; set; }
        private Queue<string> infoMsgQue;

        private int _PushTime;

        private StatusBarService() {
            this.PermMsg = string.Empty;
            this.ShowMsg = string.Empty;
            this.infoMsgQue = new Queue<string>();
            this._PushTime = 0;
        }

        public void Initialize() {
            this.SetPermanentlyMsg( RegularMessages.Initialize );
            this.PushTime = 3000;
        }

        public string Message {
            get {
                return this.ShowMsg;
            }
            private set {
                this.ShowMsg = value;
                this.RaisePropertyChanged();
            }
        }

        public int PushTime {
            get {
                return this._PushTime;
            }
            set {
                if ( value < 0 ) {
                    throw new ArgumentOutOfRangeException( "Can not under the zero in value." );
                }
                this._PushTime = value;
            }
        }

        public void SetPermanentlyMsg( string msg ) {

            this.PermMsg = msg;

            //キューが空っぽなら
            if ( this.infoMsgQue.Count == 0 ) {
                this.Message = this.PermMsg;
            }
        }

        public void SetInfomationMsg( string msg ) {

            //キューにメッセージを入れる
            this.infoMsgQue.Enqueue( msg );

            //キューの要素が唯一なら
            if ( this.infoMsgQue.Count == 1 ) {
                //workerを起動する
                Task.Run( () => this.InfoMsgUpdata() );
            }

        }

        private async void InfoMsgUpdata() {

            while (true) {

                //Messageを表示
                this.Message = this.infoMsgQue.Peek();

                //指定時間待つ
                await Task.Delay( this.PushTime );

                //キューからデータを削除
                this.infoMsgQue.Dequeue();

                //キューが空なら抜ける
                if ( this.infoMsgQue.Count == 0 ) {
                    break;
                }

            }

            //デフォルトMessageを表示
            this.Message = this.PermMsg;

        }

        /// <summary>
        /// アプリケーションの状態を示す規定のMessage文字列
        /// </summary>
        public struct RegularMessages {
            /// <summary>
            /// 初期化中...
            /// </summary>
            public static string Initialize { get; } = "初期化中...";
            /// <summary>
            /// 準備完了
            /// </summary>
            public static string Ready { get; }      = "準備完了";
        }

    }
}
