﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Livet;
using Suima.Settings;
using System.Collections.ObjectModel;
using CTC.ViewModel.Automations;

namespace CTC.Services {
    public class AutoTypeRemoveService : Base.AutomaticBaseService<AutoTypeRemoveServiceSetting> {

        private static volatile AutoTypeRemoveService _Current = null;
        public static AutoTypeRemoveService Current {
            get {
                if (_Current == null) {
                    _Current = new AutoTypeRemoveService();
                }
                return _Current;
            }
        }

        private AutoTypeRemoveService() {

        }

        public ObservableCollection<string> FormatList {
            get => Setting?.FormatList;
        }

        protected override void InitializeCore() {
            RaisePropertyChanged( nameof( IsEnabled ) );
            RaisePropertyChanged( nameof( FormatList ) );
        }

        protected override void HistryUpdataCore(Model.ClipboardData data) {

            if (FormatList == null) { return; }

            if (data.Formats.Where( i => FormatList.Any( f => f == i ) ).Any()) {
                History.SetHistory( History.CurrentData.FormatsRemove( FormatList ) );
                History.CurrentSave();
            }

        }
    }
}
