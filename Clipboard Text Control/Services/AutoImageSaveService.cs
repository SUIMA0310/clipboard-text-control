﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.Model;

namespace CTC.Services {
    public class AutoImageSaveService : Base.AutomaticBaseService<AutoImageSaveServiceSetting> {

        private static AutoImageSaveService _Current = null;
        public static AutoImageSaveService Current {
            get {
                if (_Current == null) {
                    _Current = new AutoImageSaveService();
                }
                return _Current;
            }
        }

        private ImageSaveCore SaveCore = null;

        private AutoImageSaveService() : base() {

        }

        protected override void InitializeCore() {
            this.SaveCore = new ImageSaveCore( this.Setting );
            this.SaveCore.OverwriteBehavior = ImageSaveCore.Overwrite.Rename;
            this.SaveCore.ShowErrorDialog = false;
        }

        protected override void HistryUpdataCore(ClipboardData data) {

            if (!data.IsImage) { return; }

            try {

                this.SaveCore.Save( data.Image );

            } catch {
                StatusBarService.Current.SetInfomationMsg(
                    "イメージの保存に失敗しました。"
                    );
                return;
            }

            StatusBarService.Current.SetInfomationMsg(
                "イメージを保存しました。"
                );

        }

    }
}
