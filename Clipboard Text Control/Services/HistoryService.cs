﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using CTC.Model;
using Suima.Collections;
using Suima.Extensions;
using Suima.Settings;
using Livet;
using Livet.EventListeners;
using MetroTrilithon.Lifetime;

namespace CTC.Services {

    public class HistoryService : NotificationObject, Base.IService {

        public static HistoryService Current { get; } = new HistoryService();

        private ClipboardData _CurrentData = ClipboardData.Empty;
        private HistorySetting Setting;
        private int HotKeyIndex = 0;

        public HistoryService() {

            this.History = new ObservableHistoryQueue<ClipboardData>();

            SettingMgr.Current.GetSetting<HistorySetting>((s) => {
                this.Setting = s;
                this.History.MaxCount = s.MaxCount;
            });

        }

        public void Initialize() {

            

            HotkeyMgrService.Current.Register(
                Suima.Windows.Win32.ModKeys.CONTROL | Suima.Windows.Win32.ModKeys.SHIFT,
                System.Windows.Forms.Keys.V,
                (s, e) => {
                    if ( History.Count == 0 ) { return; }
                    this.CurrentData = ((IList<ClipboardData>)History)[HotKeyIndex];
                    HotKeyIndex = ( HotKeyIndex + 1 ) % History.Count; 
                } );

            HotkeyMgrService.Current.Register(
                Suima.Windows.Win32.ModKeys.CONTROL | Suima.Windows.Win32.ModKeys.SHIFT,
                System.Windows.Forms.Keys.C,
                (s, e) => {
                    this.CurrentSave();
                } );

            this.DispatcherHistory = new DispatcherCollection<ClipboardData>( this.History, DispatcherHelper.UIDispatcher );
            RaisePropertyChanged( nameof( this.DispatcherHistory ) );

            var listener = new PropertyChangedEventListener( ClipboardIOService.Current );
            listener.RegisterHandler(
                nameof( ClipboardIOService.Current.Clipboard ),
                ( s, e ) => {
                    SetHistory();
                } );

            SetHistory();

        }

        public event EventHandler Updata;

        private void OnUpdata() {
            Updata?.Invoke( this, null );
        }

        //Clipboardの履歴を保存する
        public ObservableHistoryQueue<ClipboardData> History { get; private set; }

        //UIThread以外からの変更を許可する
        public DispatcherCollection<ClipboardData> DispatcherHistory { get; private set; }

        //選択中のデータ
        public ClipboardData CurrentData {
            get => this._CurrentData;
            set {
                if ( this._CurrentData == value ) { return; }
                this._CurrentData = value ?? ClipboardData.Empty;
                RaisePropertyChanged();
            }
        }

        private void SetHistory() {

            this.SetHistory( ClipboardIOService.Current.Clipboard );

        }

        public void SetHistory( ClipboardData data ) {

            if ( data == null ) { return; }
            if ( data == ClipboardData.Empty ) { return; }

            this.HotKeyIndex = 0;

            //if (History.Any( a => a.SequenceNumber == data.SequenceNumber )) { return; }
            if (History.Any( a => a.ID == data.ID )) {
                var select = History.Where( a => a.ID == data.ID ).Single();
                History.Remove( select );
                this.DispatcherHistory.Add( data );
                return;
            }
            
            this.CurrentData = data;
            this.DispatcherHistory.Add( data );
            OnUpdata();

            StatusBarService.Current.SetInfomationMsg( $"{data.ToString().Round( 50, "..." )} をCopyしました" );
            ShowDebugInfo( data );

        }

        public void CurrentSave() {

            ClipboardIOService.Current.SetClipboardData( this.CurrentData );
            StatusBarService.Current.SetInfomationMsg( $"{this.CurrentData.ToString().Round( 50, "..." )} をClipboardに転送しました" );

        }

        public bool IsMaxCountEnabled {
            get => Setting?.IsSet ?? false;
            set {
                if (Setting.IsSet == value) { return; }
                Setting.IsSet = value;
                History.MaxCount = Setting.MaxCount;
                RaisePropertyChanged();
            }
        }

        public int MaxCount {
            get => Setting?.Count ?? 0;
            set {
                if (Setting.Count == value) { return; }
                Setting.Count = value;
                History.MaxCount = Setting.MaxCount;
                RaisePropertyChanged();
            }
        }

        #region Debug

        [System.Diagnostics.Conditional( "DEBUG" )]
        private void ShowDebugInfo( ClipboardData data ) {

            System.Diagnostics.Debug.WriteLine(
                $"History data : {data.ToString()}" );
            System.Diagnostics.Debug.WriteLine(
                $"Is Copy Failed  => {data.IsCopyFailed}" );
            System.Diagnostics.Debug.WriteLine(
                $"Sequence Number => {data.SequenceNumber}" );
            System.Diagnostics.Debug.WriteLine(
                $"Data Formats ⇩" );
            foreach (var item in data.Data.GetFormats( false )) {
                System.Diagnostics.Debug.WriteLine( $"  {item}" );
            }

            System.Diagnostics.Debug.WriteLine( "" );

        }

        #endregion

    }

    [Setting]
    [DataContract]
    public class HistorySetting {

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public bool IsSet { get; set; }

        public int? MaxCount => IsSet ? (int?)Count : null;

    }
}
