﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTC.ViewModel.Analyzes;
using Suima.Settings;
using System.Runtime.Serialization;
using CTC.Model;

namespace CTC.Services {

    [Setting]
    [DataContract]
    public class AutoImageSaveServiceSetting : ImageSaveSetting, Base.IAutomaticBaseSetting {

        [DataMember]
        public bool IsEnabled { get; set; }

    }

}
