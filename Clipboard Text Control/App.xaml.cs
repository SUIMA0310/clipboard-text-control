﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using CTC.ViewModel;
using Livet;
using MetroRadiance.UI;
using MetroTrilithon.Lifetime;
using MetroTrilithon.Mvvm;
using Suima.Settings;

namespace CTC {

    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application, INotifyPropertyChanged, IDisposableHolder {

        #region Public Property

        /// <summary>
		/// 現在の <see cref="AppDomain"/> の <see cref="App"/> オブジェクトを取得します。
		/// </summary>
		public static App Instance => Current as App;

        #endregion

        #region View Models

        public MainWindowViewModel MainWindowViewModel { get; private set; }

        #endregion

        /// <summary>
        /// アプリケーションが開始される時のイベント。
        /// </summary>
        /// <param name="e">イベント データ。</param>
        protected override void OnStartup(StartupEventArgs e) {

            #region 多重起動チェック
#if !DEBUG
            var appInstance = new MetroTrilithon.Desktop.ApplicationInstance().AddTo( this );

            //多重起動チェック
            if (appInstance.IsFirst) {
                //初回起動の場合
                appInstance.CommandLineArgsReceived += this.CommandLineArgsReceived;
            } else {
                //既に起動済みの場合
                appInstance.SendCommandLineArgs( e.Args );
                this.Shutdown();
                return;
            }
#endif
            #endregion

            //Dispatcherの登録
            DispatcherHelper.UIDispatcher = this.Dispatcher;

            SettingMgr.Current.FilePath = CTC.Properties.Settings.Default.SettingPsth;
            SettingMgr.Current.Load();

            base.OnStartup( e );

            GCSettings.LargeObjectHeapCompactionMode 
                = GCLargeObjectHeapCompactionMode.CompactOnce;

            //Themeの変更を有効にする
            ThemeService.Current.Register( this, Theme.Dark, Accent.Blue ).AddTo( this );

            this.MainWindowViewModel = new MainWindowViewModel {
                HistoryViewModel    = new HistoryViewModel(),
                AnalyzeViewModel    = new AnalyzeViewModel(),
                AutomationViewModel = new AutomationViewModel(),
                SettingsViewModel   = new SettingsViewModel(),
            };

            //Windowを表示
            this.MainWindow = new View.MainWindow { DataContext = this.MainWindowViewModel };
            var splash = new View.SplashWindow();
            splash.ShowDialog();
            this.MainWindow.Show();

            Services.HotkeyMgrService.Current.Register(
                Suima.Windows.Win32.ModKeys.CONTROL | Suima.Windows.Win32.ModKeys.SHIFT,
                System.Windows.Forms.Keys.V,
                (sender, args) => {
                    this.Dispatcher.Invoke( () => {
                        this.MainWindowViewModel?.Activate();
                    });
                });

            Services.HotkeyMgrService.Current.Register(
                Suima.Windows.Win32.ModKeys.CONTROL | Suima.Windows.Win32.ModKeys.SHIFT,
                System.Windows.Forms.Keys.C,
                (sender, args) => {
                    this.Dispatcher.Invoke( () => {
                        this.MainWindowViewModel?.Deactive();
                    } );
                } );

            //Serviceの初期化
            Services.ClipboardIOService.Current.AddTo( this ).Initialize();
            Services.HotkeyMgrService.Current.AddTo( this ).Initialize();
            Services.HistoryService.Current.Initialize();
            Services.StatusBarService.Current.Initialize();
            Services.AutoTypeRemoveService.Current.Initialize();
            Services.AutoImageSaveService.Current.Initialize();

            Services.StatusBarService.Current.SetPermanentlyMsg
                ( Services.StatusBarService.RegularMessages.Ready );

        }

        /// <summary>
        /// アプリケーションが終了する時のイベント。
        /// </summary>
        /// <param name="e">イベント データ。</param>
        protected override void OnExit(ExitEventArgs e) {

            base.OnExit( e );
            try {
                Clipboard.Flush();
            } catch { }

            SettingMgr.Current.Save();
            CTC.Properties.Settings.Default.Save();

            ( (IDisposable)this ).Dispose();

        }

        private void CommandLineArgsReceived(object s, MetroTrilithon.Desktop.MessageEventArgs e) {
            // 多重起動を検知したら、メイン ウィンドウを最前面に出す
            this.Dispatcher.Invoke( () => {
                this.MainWindowViewModel?.Activate();
            } );
            #region comment
            // ↑のActivateを実行するには
            // WindowのXamlに、
            // WindowState="{Binding WindowState, Mode=OneWayToSource}" とか
            // <i:Interaction.Triggers>
            //   < livet:InteractionMessageTrigger Messenger = "{Binding Messenger}"
            //                                     MessageKey = "Window.WindowAction" >
            //     < livet:WindowInteractionMessageAction InvokeActionOnlyWhenWindowIsActive = "False" />
            //   </ livet:InteractionMessageTrigger >
            // </i:Interaction.Triggers>
            // を入れなきゃダメだからね！！
            #endregion
        }

        #region INotifyPropertyChanged members

        private event PropertyChangedEventHandler PropertyChangedInternal;

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
            add { PropertyChangedInternal += value; }
            remove { PropertyChangedInternal -= value; }
        }

        private void RaisePropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChangedInternal?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region IDisposable members

        private readonly LivetCompositeDisposable compositeDisposable = new LivetCompositeDisposable();

        ICollection<IDisposable> IDisposableHolder.CompositeDisposable => this.compositeDisposable;

        void IDisposable.Dispose() {
            this.compositeDisposable.Dispose();
        }

        #endregion

    }
}
